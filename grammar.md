---
layout: page
title: Grammar
description: 
permalink: /grammar/
order: 1
---

## Basics

**Write for all readers.** Some people will read every word you write. Others will just skim. Help everyone read better by grouping related ideas together and using descriptive headers and subheaders.

**Focus your message.** Create a hierarchy of information. Lead with the main point or the most important content, in sentences, paragraphs, sections, and pages.

**Be concise.** Use short words and sentences. Avoid unnecessary modifiers.

**Be specific.** Avoid vague language. Cut the fluff.

**Be consistent.** Stick to the copy patterns and style points outlined in this guide.

## Abbreviations and acronyms

If there’s a chance your reader won’t recognise an abbreviation or acronym, spell it out the first time you mention it. Then use the short version for all other references. If the abbreviation isn’t clearly related to the full version, specify in parentheses.

- First use: User Acceptance Test (UAT)
- Second use: UAT

- First use: Coordinated Universal Time (UTC)
- Second use: UTC

If the abbreviation or acronym is well-known, like API or HTML, use it instead (and don’t worry about spelling it out).

If the abbreviation or acronym is used on a website, use an ```<abbr>``` tag.

```The <abbr title="World Health Organization">WHO</abbr> was founded in 1948.```

## Active voice

Use active voice. Avoid passive voice.

In active voice, the subject of the sentence does the action. In passive voice, the subject of the sentence has the action done to it.

- Yes: Marti logged in to the account.
- No: The account was logged in to by Marti.

Words like “was” and “by” may indicate that you’re writing in passive voice. Scan for these words and rework sentences where they appear.

One exception is when you want to specifically emphasise the action over the subject. In some cases, this is fine.

- Major changes have been pushed by our team.

## Capitalisation

We use a few different forms of capitalisation. Title case capitalises the first letter of every word except articles, prepositions, and conjunctions. Sentence case capitalises the first letter of the first word.

When writing out an email address or website URL, use all lowercase.

- spectrum@grayscale.com.hk
- grayscale.com.hk

Don’t capitalise random words in the middle of sentences.

## Contractions

They’re great! They give your writing an informal, friendly tone. In most cases, use them as you see fit.

Always remember to use a quote (’) instead of a prime (').

## Emoji

Emoji are a fun way to add humour and visual interest to your writing, but use them infrequently and deliberately.

Remember not to place any emoji in the middle of a word.

## Pronouns

If your subject’s gender is unknown or irrelevant, use “they,” “them,” and “their” as a singular pronoun. Use “he/him/his” and “she/her/her” pronouns as appropriate. Don’t use “one” as a pronoun.

## Quotes

When quoting someone in a blog post or other publication, use the present tense.

- “Using MailChimp has helped our business grow,” says Jamie Smith.

## Slang and jargon

Write in plain English. If you need to use a technical term, briefly define it so everyone can understand.

## Write positively

Use positive language rather than negative language. One way to detect negative language is to look for words like “can’t,” “don’t,” etc.

- Yes: To get a doughnut, stand in line.
- No: You can’t get a doughnut if you don’t stand in line.
