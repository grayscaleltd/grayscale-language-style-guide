---
layout: page
title: Format
description: There are lots of ways to format numbers, dates, etc., and it is confusing to see the format constantly changing. This section defines the formatting instructions to keep the format consistent.
permalink: /format/
order: 3
---

## Body text formatting

For Latin texts, left-align body texts by default, centre-align when used for blockquotes or subheaders, never right-aligned or justified.

For [CJK](/terminology/#cjk) texts, it’s generally suggested to justify texts for better visual balance.

Leave one space between sentences, never two.

<!-- ## Compound word

Never split a compound word that’s been written as a single word.

- Yes: Upgrade
- No: Up grade

Add a hyphen (-) between words of a compound verb by default. However, if the last word is an adverb, skip the hyphen and keep a space between.

- Yes: Double-click the icon.
- No: Doubleclick the icon.
- No: Double click the icon.

- Yes: You can log in to the admin panel.
- No: You can login to the admin panel.
- No: You can log-in to the admin panel.

Always hyphenate compound adjectives and adverbs; keep a space between compound nouns.

- A one-page website
- Created in-house
- The log in button -->

## Dates

Generally, spell out the day of the week and the month and always place the day before the month. Abbreviate only if space is an issue.

Always write years in numeric form.

- Saturday, 24 January, 2018
- Sat, 24 Jan, 2018
- 26/05/2018
- 2018 is the year when this blog is written.

## Decimals and fractions

Spell out fractions.

- Yes: two-thirds
- No: 2/3

Use word form or decimal points when a number can’t be easily written out as a fraction, like 9 out of 14 or 47.2.

## File extensions

When referring generally to a file extension type, use all uppercase without a period. Add a lowercase “s” to make plural.

- GIF
- PDF
- HTML
- JPGs

When referring to a specific file, the filename should be lowercase:

- slowclap.gif
- GSBenefits.pdf
- ben-twitter-profile.jpg
- ilovedoughnuts.html

## Money

When writing the currency as an item, make sure you first mention the currency abbreviations, then use the currency symbol before the amount if it exists.

If the currency is in a sentence, put the currency abbreviation after the amount.

Always include a decimal and number of cents if more than 0.

 Amount (HKD) | Amount (EUR) | Amount (CHF) 
-------------:|-------------:|-------------:
       $3,000 |       €3,000 |        3,000 
      $20,000 |      €20,000 |       20,000 
     $120,100 |     €120,100 |      120,000 

- The total cost is 120,000 HKD.

## Names, titles, and countries

The first time you mention a person in writing, refer to them by their first and last names. On all other mentions, refer to them by their first name.

Capitalise the names of departments and teams (but not the word “team” or “department”).

- Account team
- Support department

Capitalise individual job titles when referencing to a specific role. Don't capitalise when referring to the role in general terms.

- Our new Front-end Developer starts today.
- All the managers ate doughnuts.

Write out a country’s full name on its first mention. On subsequent mentions, common abbreviation (European Union, EU; United Kingdom, UK; United States of America, USA) can then be used.

## Numbers

Use numeral for a number if it’s larger than or equal to 10, or if it’s an ordinal. Otherwise, spell it out.

- Nine new employees started on Monday, and 12 start next week.
- I ate three doughnuts at Coffee Hour.
- Meg won 1st place in last year’s Walktober contest.

Always use the comma as the thousand separator, but don’t put a space after the comma.

- Yes: 1,000
- No: 1, 000

- Yes: 150,000
- No: 150 000

Write out big numbers in full. Abbreviate them if there are space restraints, as in a tweet or a chart: 1k, 150k.

## Percentages

Use the % symbol instead of spelling out “percent.”

## Ranges and spans

Use an en dash (–) to indicate a range or span of numbers.

- It takes 20–30 days.

## Telephone numbers

Start with a country code and use space between numbers.

- +852 5804 3077
- +1 404 123 4567

## Time

Use numerals and am or pm. Don’t use minutes (:00) for on-the-hour time. 

- 7am 
- 7:30pm

Use an en dash (–) between times to indicate a time period. 

- 8am–11am
- 7am–10:30pm

Specify time zones when writing to people who’s not from the same time zone as Hong Kong Time.
When referring to international time zones, spell them out: Nepal Standard Time, Australian Eastern Time. If a time zone does not have a set name, use its Coordinated Universal Time (UTC) offset.

- 7:30pm Eastern Standard Time
- 7:30pm UTC−05:00

Abbreviate decades when referring to those within the past 100 years.

- the 00s
- the 90s

When referring to decades more than 100 years ago, be more specific.

- the 1910s
- the 1890s

## URLs and websites

Capitalise the names of websites and web publications. Don’t italicise.

Avoid spelling out URLs, but, when you need to, leave off the http://www.

Refer to Grayscale as “we,” not “it.”

Honour companies’ own names for themselves and their products. Go by what’s used on their official website.

- MailChimp
- iPad
- YouTube

Refer to a company or product as “it” (not “they”).
