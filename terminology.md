---
layout: page
title: Terminology
description: There are often mix-ups of technological terms during communications. In this section, terminologies frequently used are defined and standardised.
permalink: /terminology/
order: 4
---

## Bug

An error or a flaw which makes a software or a system produces an incorrect result deviating from any predefined requirement.

## CJK

A collective term for the Chinese, Japanese, and Korean languages.

All CJK languages share a common background in their writing systems. The scripts are collectively known as CJK characters.

## Colophon

A term borrowed from publishing. It contains attributions of and information about producing a website.

Standard content consists the line “Designed and developed in Hong Kong by [Grayscale](https://grayscale.com.hk/website-development-hong-kong/).”.

## Content management system

A software that facilitates creating, editing, organising, and publishing content.

This is often referred to as CMS.

## E-commerce

A type of business model which enables a firm or individual to conduct business over the internet.

A hyphen must be placed between the words even though e-commerce is a compound noun.

Never refer to a platform or website as e-commerce alone. Call them e-commerce platform or e-commerce website.

## Firefox

A web browser developed by Mozilla.

The name must be spelled exactly as Firefox.

## Front-end developer

A web developer who converts visual designs into accessible components and features on a website.

When referred to as a specific title, it should be written as Front-end Developer.

## Google Analytics

A service for tracking website user behaviour.

This is often referred to as GA.

## Grayscale

A brand name referring to the company Grayscale Limited.

Use Grayscale Limited (spelled in full) when first used in legal documents. Otherwise, refer to the company as Grayscale.

- Yes: Signed by Grayscale Limited.
- No: Signed by Grayscale Ltd..

- Yes: Design is inherently a structured process, especially at Grayscale.
- No: Design is inherently a structured process, especially at Grayscale Limited.
- No: Design is inherently a structured process, especially at grayscale.

## GrayscaleCare

A maintenance package offered by Grayscale to make sure clients’ WordPress and its plugins are up-to-date.

The name must be spelled exactly as GrayscaleCare.

## Log in

The action of getting into a system with credentials.

Don’t use any other terms to describe the action.

- Yes: Log in to WordPress
- Yes: Click the log in button
- No: Login to WordPress
- No: Log on to WordPress
- No: Sign on to WordPress

## Log out

The action of exiting a logged in system.

Don’t use any other terms to describe the action.

- Yes: Log out
- Yes: Click the log out button
- No: Logout of WordPress
- No: Log off WordPress
- No: Sign out of WordPress

## Made with ❤︎ by Grayscale.

A legacy line placed in the footer of a client’s website unless otherwise specified. This has mostly been replaced by the colophon.

The unicode glyph “❤︎” must be used in the line.

- Yes: Made with ❤︎ by Grayscale.
- No: Made with <3 by Grayscale.

## Minimum viable product

The base product needed to solve a specific problem.

This is often referred to as MVP.

Always try to refer to specific tasks or items when using the word MVP, as the scope of the term is usually not well-defined.

- Yes: We’ve built them a website which solves […].
- No: We’ve built them an MVP.

## Non-governmental organisation

A non-profit group which functions without interference from governments, organised around specific social, or political issues.

This is often referred to as NGO.

Don’t confuse non-governmental organisation (NGO) with non-profit organisation (NPO)—they shouldn’t be used interchangeably.

An NGO is non-profit by nature; an NPO might not be established to support a social-political cause.

## Request for proposal

A document to solicit proposal from potential vendors, this is sent out during the early stage of a procurement process.

This is often referred to as RFP.

## Scope of Work

A description where the work to be performed is described.

This is often referred to as SOW.

## Secure Sockets Layer

A security technology that ensures data transmitted between the web server and a visitor’s browser remains private and whole.

This is often referred to as SSL.

## Wireframe

A low-fidelity impression of a website or app’s layout. 

This is often described as website blueprints as it shows where components are approximately going to be.

## WordPress

A free and open-source content management system (CMS) based on PHP and MySQL.

The name must be spelled exactly as WordPress.

Never refer to WordPress as WordPress CMS.

- Yes: We use WordPress as our CMS.
- No: We use WordPress CMS.
- Worst: We use WordPress CMS as our CMS.

## WordPress template

A pattern written in PHP, used for producing a page.

This is often referred to as template.

This term shouldn’t be used interchangeably with WordPress theme. WordPress templates are part of a WordPress theme.

See “WordPress theme” for more technicalities in using the term.

## WordPress theme

A collection of files that works together to modify a WordPress site’s appearance and function.

This is often referred to as theme.

This term shouldn’t be used interchangeably with WordPress template. WordPress theme contains WordPress templates.

You should never say “we don’t use theme” or “we don’t use any pre-made theme,” as we do build WordPress sites based on an in-house custom theme.

- We don’t use theme from any third parties; our themes are created in-house.
- We create themes based on our custom design.

## Words to prevent

**break up**—this is used to refer to a split of a relationship; use “outline,” or “break down” for terms and groups.

**in-between**—the word between already exhibits the inclusiveness, this makes the word in redundant.

**Web**—Don’t capitalise first letter if it’s not the first word of the sentence.
