const sectionTitles = document.querySelectorAll('.content h2');
const articleMenu = document.querySelectorAll('aside.menu nav ul')[0];

const prependSectionToMenu = function(obj) {
  let title = obj.textContent;
  let titleUrl = obj.getAttribute('id');
  let navItem = document.createElement('li');
  let anchor = document.createElement('a');

  anchor.innerHTML = title;
  anchor.setAttribute('href', `#${titleUrl}`);

  navItem.appendChild(anchor);

  articleMenu.insertBefore(navItem, articleMenu.firstChild);
};

if (articleMenu) {
  for (var i = sectionTitles.length - 1; i >= 0; i--) {
    prependSectionToMenu(sectionTitles[i]);
  }

  let nav = new StickySidebar('.article-content .menu_inner', {
    topSpacing: 0,
    bottomSpacing: 0,
    containerSelector: '.article-content',
    innerWrapperSelector: '.article-content nav'
  });
}
