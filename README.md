Grayscale Language Style Guide
----

To make our communication clear and consistent, we have compiled a language style guide applied it to all our communication materials. 

The Grayscale Language Style Guide defines stylistic instructions across four categories: Grammar, Punctuation, Terminology, and Format.

Parts of this style guide are derived from the [Mailchimp Content Style Guide](https://styleguide.mailchimp.com/grammar-and-mechanics/).

## Grammar
This section defines how we structure our sentences to make them easy to understand by everyone.

## Punctuation
This section defines how words or sentences should be meaningfully linked together with punctuations.

## Terminology
This section standardises the choice of terminology used in Grayscale to avoid confusion.

## Format
This section standardises the format regarding time, date, number, etc.

Made with ❤︎ by Grayscale. This style guide is available under a [Creative Commons Attribution-NonCommercial 4.0 International License](https://creativecommons.org/licenses/by-nc/4.0/).
