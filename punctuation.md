---
layout: page
title: Punctuation
description: 
permalink: /punctuation/
order: 2
---

## Ampersands

Don't use ampersands unless one is part of a company or brand name.

- Ben and Dan
- Ben & Jerry’s
- People, Places, and Things

## Apostrophes

The apostrophe’s most common use is making a word possessive. If the word already ends in an s and it’s singular, you also add an ‘s. If the word ends in an s and is plural, just add an apostrophe.

- The doughnut thief ate Sam’s doughnut.
- The doughnut thief ate Chris’s doughnut.
- The doughnut thief ate the managers’ doughnuts.

Apostrophes can also be used to denote that you’ve dropped some letters from a word, usually for humour or emphasis. This is fine, but do it sparingly.

## Colons

Use a colon (rather than an ellipsis, em dash, or comma) to offset a list.

- Erin ordered 3 kinds of doughnuts: glazed, chocolate, and pumpkin.

## Commas

When writing a list, use the Oxford comma. 

- Yes: David admires his parents, Oprah, and Justin Timberlake.
- No: David admires his parents, Oprah and Justin Timberlake.

Otherwise, use common sense. If you’re unsure, read the sentence out loud. Where you find yourself taking a breath, use a comma.

## Dashes and hyphens

Use a hyphen (-) without spaces on either side to link words into single phrase .

- first-time user

Use an en dash (–) to indicate a span, a range, a conflict, or a connection.

- Monday–Friday
- A Hong Kong–Singapore flight

To type the en dash on a Mac, hold ```option``` + ```-```.

Use an em dash (—) to emphasise the conclusion of your sentence, or join two related phrases. If a complete sentence follows the em dash, capitalise the first word.

Use a true em dash, not hyphens (- or --).

- Austin thought Brad was the doughnut thief, but he was wrong—it was Lain.
- I was faced with a dilemma—I wanted a doughnut, but I’d just eaten a bagel.

To type the em dash on a Mac, hold ```option``` + ```shift``` + ```-```.

## Ellipses

Ellipses (…) can be used to indicate that you’re trailing off before the end of a thought. Use them sparingly. Don’t use them for emphasis or drama, and don’t use them in titles or headers.

- “Where did all those doughnuts go?” Christy asked. Lain said, “I don't know…”

Ellipses, in brackets, can also be used to show that you're omitting words in a quote.

- “When in the Course of human events it becomes necessary for one people to dissolve the political bands which have connected them with another and to assume among the powers of the earth, […] a decent respect to the opinions of mankind requires that they should declare the causes which impel them to the separation.”

Don’t use three periods (...) in place of an ellipsis.

To type the ellipses on a Mac, hold ```option``` + ```;```.

## Exclamation points

Use exclamation points sparingly, and never more than one at a time. They’re like high fives—A well-timed one is great, but too many can be annoying.

Exclamation points go inside quotation marks. They go outside parentheses when the parenthetical is part of a larger sentence, and inside parentheses when the parenthetical stands alone.

Never use exclamation points in failure messages or alerts. When in doubt, avoid!

## Interrobang

Interrobang (?!) can be used to express shock or excitement in a form of question. Use this even more sparingly than exclamation point—only when it’s absolutely needed.

Interrobangs go inside quotation marks. They go outside parentheses when the parenthetical is part of a larger sentence, and inside parentheses when the parenthetical stands alone.

- The site is down?!
- The payment didn’t go through?!

In case you need to use interrobang in Spanish, you should write the reversed interrobang as “¡¿.”

## Periods

Periods go inside quotation marks. They go outside parentheses when the parenthetical is part of a larger sentence, and inside parentheses when the parenthetical stands alone.

- Christy said, “I ate a doughnut.”
- I ate a doughnut (and I ate a bagel, too).
- I ate a doughnut and a bagel. (The doughnut was Sam’s.)

Leave a single space between sentences.

## Question marks

Question marks go inside quotation marks if they’re part of the quote. They go outside parentheses when the parenthetical is part of a larger sentence, and inside parentheses when the parenthetical stands alone.

## Quotation marks

Use quotes to refer to words and letters, titles of short works (like articles and poems), and direct quotations.

Periods and commas go within quotation marks. Question marks within quotes follow logic—if the question mark is part of the quotation, it goes within. If you’re asking a question that ends with a quote, it goes outside the quote.

Use single quotation marks for quotes within quotes.

- Who was it that said, “A fool and his doughnut are easily parted”?
- Brad said, “A wise man once told me, ‘A fool and his doughnut are easily parted.’”

Always remember to use quotes (’) instead of primes (').

## Semicolons

Use semicolon to separate items when commas are already used in list items.

- European Union, EU; United Kingdom, UK; United States, US.

When using semicolons in narrative sentence, go easy. They usually support long, complicated sentences that could easily be simplified. Try a comma instead, or simply start a new sentence.
