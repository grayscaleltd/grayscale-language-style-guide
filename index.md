---
layout: home
title: Grayscale Language Style Guide
subtext: Make your communication clear and consistent
description: To make our communication clear and consistent, we have compiled a language style guide for all our communication materials. 
---

The Grayscale Language Style Guide defines stylistic instructions across four categories: Grammar, Punctuation, Format, and Terminology.

Parts of this style guide are derived from the [MailChimp Content Style Guide](https://styleguide.mailchimp.com/grammar-and-mechanics/).

## [Grammar](/grammar)
This section defines how we structure our sentences to make them easy to understand by everyone.

## [Punctuation](/punctuation)
This section defines how words or sentences should be meaningfully linked together with punctuations.

## [Format](/format)
This section standardises the format regarding time, date, number, etc.

## [Terminology](/terminology)
This section standardises the choice of terminology used in Grayscale to avoid confusion.
